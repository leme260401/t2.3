using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieScript : MonoBehaviour
{

    private Rigidbody2D rb;
    public GameObject eg;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("GenerarZombie", 3, 2);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.left * speed;
        Physics2D.IgnoreLayerCollision(7,9,true);
        StartCoroutine("EliminarZombie");
    }

    private void GenerarZombie()
    {
        var x = eg.transform.position.x;
        var y = eg.transform.position.y;
        Rigidbody2D instance = Instantiate(rb, new Vector2(x,y), rb.transform.rotation);
    }

    IEnumerator EliminarZombie(){
        yield return new WaitForSeconds(7);
        Destroy(this.gameObject);
    }
}
