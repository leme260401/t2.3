using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NinjaScript : MonoBehaviour
{
    private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;
    private BoxCollider2D col;

    private bool puedeSaltar = false;
    private bool puedeSubirEscalera = false;
    private int speedUp2 = 7;

    public Text lifeText;
    public Text scoreText;
    private int Score = 0;
    private int Life = 3;

    private float alturaMax;
    private float altura;
    private Vector3 posActual;
    private bool resetAltura = false;

    public GameObject kunaiRight;
    public GameObject kunaiLeft;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        col = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

        lifeText.text = "VIDA: " + Life;
        scoreText.text = "PUNTAJE: " + Score;

        //DESPLAZARSE
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX= false;
            setRunAnimation();
            rb2d.velocity = new Vector2(10,rb2d.velocity.y);
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setRunAnimation();
            rb2d.velocity = new Vector2(-10,rb2d.velocity.y);
        }

        //SALTAR
        if (Input.GetKeyDown(KeyCode.Space) && puedeSaltar)
        {
            setJumpAnimation();
            float speedUp = 25;
            rb2d.velocity = Vector2.up * speedUp;
            puedeSaltar = false;
        }

        //SUBIR ESCALERA
        if (Input.GetKeyDown(KeyCode.UpArrow) && puedeSubirEscalera)
        {
            setClimbAnimation();
            rb2d.gravityScale = 0;
            rb2d.velocity = Vector2.up * speedUp2;
            DeshabilitarColisionConSuelo();
        }

        //BAJAR ESCALERA Y DESLIZARSE
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if(puedeSubirEscalera)
            {
                setClimbAnimation();
                rb2d.gravityScale = 10;
                rb2d.velocity = Vector2.down * speedUp2;
                DeshabilitarColisionConSuelo();
            }

            if(!puedeSubirEscalera)
            {
                setSlideAnimation();
            }
        }

        //PLANEAR
        if(Input.GetKey(KeyCode.Z) && puedeSaltar == false)
        {
            setGlideAnimation();
            rb2d.gravityScale = 1;
            alturaMax = 0;
        }

        //CALCULAR ALTURA
        if(puedeSaltar == false)
        {
            posActual = transform.position;
	        altura = posActual.y;
	        if(alturaMax <= altura){
		        alturaMax = posActual.y;
	        }
	        if(resetAltura){
		        ResetAlturaMax();
	        }
            Debug.Log("Altura: "+alturaMax.ToString());
            
        }

        //DISPARAR
        if(Input.GetKeyDown(KeyCode.X))
        {
            if(!sr.flipX)
            {
                var position = new Vector2(transform.position.x + 1, transform.position.y);
                Instantiate(kunaiRight, position, kunaiRight.transform.rotation);
            }
            else
            {
                var position = new Vector2(transform.position.x - 2, transform.position.y);
                Instantiate(kunaiLeft, position, kunaiLeft.transform.rotation);
            }
        }

        //MORIR
        if(Life == 0)
        {
            setDeadAnimation();
            StartCoroutine("FinJuego");
        }
    }

    IEnumerator FinJuego()
    {
        yield return new WaitForSeconds(2f);
        UnityEngine.Debug.Break();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.layer == 6 || other.gameObject.layer == 7){
            puedeSaltar = true;
            rb2d.gravityScale = 8;
        }
        if(other.gameObject.tag == "Zombie")
        {
            DisminuirVidaEn1();
        }
        if(alturaMax >= 6 && other.gameObject.layer == 6)
        {
            Life = 0;
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.tag == "Escalera")
        {
            puedeSubirEscalera = true;
            rb2d.gravityScale = 0;
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Escalera")
        {
            puedeSubirEscalera = false;
            rb2d.gravityScale = 6;
            HabilitarColisionConSuelo();
        }
    }

    private void ResetAlturaMax() {
        alturaMax = altura;
        resetAltura = false;
    }

    public void DeshabilitarColisionConSuelo()
    {
        Physics2D.IgnoreLayerCollision(7,8,true);
    }

    public void HabilitarColisionConSuelo()
    {
        Physics2D.IgnoreLayerCollision(7,8,false);
    }

    public void DisminuirVidaEn1()
    {
        Life -= 1;
    }

    public void IncrementarPuntajeEn10()
    {
        Score += 10;
    }

    private void setIdleAnimation(){
        _animator.SetInteger("State",0);
    }

    private void setRunAnimation(){
       _animator.SetInteger("State",1);
    }

    private void setJumpAnimation(){
        _animator.SetInteger("State",2);
    }

    private void setSlideAnimation(){
        _animator.SetInteger("State",3);
    }

    private void setGlideAnimation(){
        _animator.SetInteger("State",4);
    }
    
    private void setClimbAnimation(){
        _animator.SetInteger("State",5);
    }

    private void setDeadAnimation(){
        _animator.SetInteger("State",6);
    }

}
